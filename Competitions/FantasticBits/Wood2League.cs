﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace CodinGame.Competitions.FantasticBits
{
    public class Wood2League
    {
        public static void Main()
        {
            var entities = new List<Entity>();
            var leftGoal = new Point(0, 3750);
            var rightGoal = new Point(16000, 3750);

            var teamId = int.Parse(Console.ReadLine() ?? "");
            Console.Error.WriteLine("TeamId: " + teamId);
            var goal = teamId == 0 ? rightGoal : leftGoal;

            while (true)
            {
                var entitiesInPlay = int.Parse(Console.ReadLine() ?? "");

                var inputtedEntites = Enumerable
                    .Range(0, entitiesInPlay)
                    .Select(i => (Console.ReadLine() ?? "").Split(' '))
                    .Select(gp => new Entity
                    {
                        Id = int.Parse(gp[0]),
                        Type = (EntityType) Enum.Parse(typeof(EntityType), gp[1].Replace("_", ""), true),
                        Position = new Point(int.Parse(gp[2]), int.Parse(gp[3])),
                        Velocity = new Point(int.Parse(gp[4]), int.Parse(gp[5])),
                        State = (State) int.Parse(gp[6])
                    })
                    .ToArray();

                if (entities.Count == 0)
                {
                    entities.AddRange(inputtedEntites);
                }
                else
                {
                    // Remove all entities which haven't been inputted.
                    var inputtedEntityIds = inputtedEntites.Select(e => e.Id);
                    entities.RemoveAll(e => !inputtedEntityIds.Contains(e.Id));

                    // Update any entities which exist.
                    foreach (var entity in entities.Where(e => inputtedEntityIds.Contains(e.Id)))
                        entity.Position = inputtedEntites.Single(e => e.Id == entity.Id).Position;
                }

                var wizards = entities.Where(e => e.Type == EntityType.Wizard);

                foreach (var wizard in wizards)
                {
                    var distanceToGoal = GetDistance(goal, wizard.Position);
                    if (wizard.HasSnaffle && distanceToGoal < 100)
                    {
                        Console.Error.WriteLine("Wizard has snaffle, and close to goal.");
                        Console.Error.WriteLine(wizard.GetPosition());
                        Console.Error.WriteLine("Goal: {0}, {1}", goal.X, goal.Y);
                        var command = "THROW " + goal.X + " " + goal.Y + " 100";
                        Console.Error.WriteLine("Command: {0}", command);
                        Console.WriteLine(command);
                        continue;
                    }

                    if (wizard.HasSnaffle)
                    {
                        Console.Error.WriteLine("Wizard has snaffle, moving to goal.");
                        Console.Error.WriteLine(wizard.GetPosition());
                        Console.Error.WriteLine("Goal: {0}, {1}", goal.X, goal.Y);
                        var command = "MOVE " + goal.X + " " + goal.Y + " 100";
                        Console.Error.WriteLine("Command: {0}", command);
                        Console.WriteLine(command);
                        continue;
                    }

                    var heldSnaffles = entities.Where(e => e.HasSnaffle).Select(e => e.HeldSnaffle).ToArray();

                    var snafflesInOrder = entities
                        .Where(e => e.Type == EntityType.Snaffle && !heldSnaffles.Contains(e))
                        .OrderBy(s => GetDistance(wizard.Position, s.Position))
                        .ToArray();

                    var targetedSnaffles = entities.Where(e => e.TargetSnaffle != null).ToArray();

                    // Closest snaffle that isn't already targeted.
                    var closestSnaffle = snafflesInOrder.First(s => !targetedSnaffles.Contains(s));

                    Console.Error.WriteLine("Wizard has targeted snaffle.");
                    wizard.TargetSnaffle = closestSnaffle;
                    Console.Error.WriteLine(wizard.GetPosition());
                    Console.Error.WriteLine(closestSnaffle.GetPosition());

                    var distanceToSnaffle = GetDistance(closestSnaffle.Position, wizard.Position);
                    Console.Error.WriteLine("Distance to snaffle: " + distanceToSnaffle);
                    if (distanceToSnaffle < 100)
                    {
                        Console.Error.WriteLine("Picked up snaffle.");
                        wizard.HeldSnaffle = closestSnaffle;
                        Console.Error.WriteLine("Picked up snaffle, now moving to goal.");
                        Console.Error.WriteLine(wizard.GetPosition());
                        Console.Error.WriteLine("Goal: {0}, {1}", goal.X, goal.Y);
                        var command = "MOVE " + goal.X + " " + goal.Y + " 100";
                        Console.Error.WriteLine("Command: {0}", command);
                        Console.WriteLine(command);
                    }

                    Console.Error.WriteLine("Moving to snaffle: ");
                    Console.Error.WriteLine(wizard.GetPosition());
                    Console.Error.WriteLine(closestSnaffle.GetPosition());
                    Console.WriteLine("MOVE " + closestSnaffle.Position.X + " " + closestSnaffle.Position.Y + " 150");
                }
            }
        }

        private static double GetDistance(Point p1, Point p2)
        {
            var x = p1.X - p2.X;
            var y = p1.Y - p2.Y;
            return Math.Sqrt(x*x + y*y);
        }

        public enum EntityType
        {
            Wizard,
            OpponentWizard,
            Snaffle,
            Bludger
        }

        public enum State
        {
            Other = 0,
            HoldingSnaffle = 1
        }

        public class Entity
        {
            public int Id { get; set; }
            public EntityType Type { get; set; }
            public Point Position { get; set; }
            public Point Velocity { get; set; }
            public State State { get; set; }
            public Entity HeldSnaffle { get; set; }
            public Entity TargetSnaffle { get; set; }

            public bool HasSnaffle
            {
                get { return HeldSnaffle != null; }
            }

            public string GetPosition()
            {
                return Type + " " + Id + ": " + Position.X + ", " + Position.Y;
            }

            public override string ToString()
            {
                return "Id: " + Id
                    + "\nType: " + Type
                    + "\nPosition: " + Position.X + ", " + Position.Y
                    + "\nVelocity: " + Velocity.X + ", " + Velocity.Y
                    + "\nState: " + State
                    + "\nHasSnaffle: " + HasSnaffle;
            }
        }
    }
}