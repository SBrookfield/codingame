﻿using System;
using System.Linq;

namespace CodinGame.Medium
{
    public class ThereIsNoSpoon
    {
        public static void Main()
        {
            var width = int.Parse(Console.ReadLine()); // the number of cells on the X axis
            var height = int.Parse(Console.ReadLine()); // the number of cells on the Y axis
            var grid = new int[height][];
            int[] pos = null;

            for (var i = 0; i < height; i++)
            {
                grid[i] = Console.ReadLine()?.ToCharArray().Select(c => c != '.' ? 1 : 0).ToArray() ?? new int[0];
                var index = Array.IndexOf(grid[i], 1);
                if (index > -1 && pos == null)
                    pos = new [] {i, index};
            }

            if (pos == null)
            {
                Console.WriteLine("-1 -1 -1 -1 -1 -1");
                return;
            }

            var none = new [] {-1, -1};
            var hasRight = pos[1] < width;
            var right = hasRight ? new[] {grid[pos[0]][pos[1] + 1]} : none;
            var hasDown = pos[0] < height;
            var down = hasDown ? new[] { grid[pos[0] + 1][pos[1]] } : none;
            var p = new Func<int[], string>(i => string.Format("{0} {1}", i[0], i[1]));

            Console.WriteLine("{0} {1} {2}", p(pos), p(right), p(down));
        }
    }
}