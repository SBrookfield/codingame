﻿using CodinGame.Competitions.FantasticBits;
using CodinGame.Easy;
using CodinGame.Medium;
using CodinGame.Tutorial;

namespace CodinGame
{
    public static class CodinGame
    {
        private static void Main()
        {
            //Onboarding.Main();
            //PowerOfThor.Main();
            //SkynetTheCasm.Main();
            //MarsLander.Main();
            //Temperatures.Main();
            //AsciiArt.Main();
            //ChuckNorris.Main();
            //MimeType.Main();
            //Defibrillators.Main();
            //HorseRacingDuals.Main();
            //ThereIsNoSpoon.Main();

            Wood2League.Main();
        }
    }
}