﻿using System;

namespace CodinGame.Tutorial
{
    public class Onboarding
    {
        public static void Main()
        {
            while (true)
            {
                var enemy1 = Console.ReadLine();
                var dist1 = int.Parse(Console.ReadLine());
                var enemy2 = Console.ReadLine();
                var dist2 = int.Parse(Console.ReadLine());

                Console.WriteLine(dist1 < dist2 ? enemy1 : enemy2);
            }
        }
    }
}