﻿//using System;
//using System.Drawing;
//using System.Linq;

//namespace CodinGame.Easy
//{
//    public class MarsLander
//    {
//        public static void Main()
//        {
//            var surfacePointCount = int.Parse(Console.ReadLine());
//            var surface = new Point[surfacePointCount];

//            for (var i = 0; i < surfacePointCount; i++)
//            {
//                var input = Console.ReadLine().Split(' ');
//                surface[i] = new Point(int.Parse(input[0]), int.Parse(input[1]));
//            }

//            var landingHeight = surface
//                    .GroupBy(p => p.Y)
//                    .Where(g => g.Count() > 1)
//                    .SelectMany(g => g)
//                    .First()
//                    .Y;

//            while (true)
//            {
//                var input = Console.ReadLine().Split(' ');
//                var position = new Point(int.Parse(input[0]), int.Parse(input[1]));
//                var horizontalSpeed = int.Parse(input[2]); // the horizontal speed (in m/s), can be negative.
//                var verticalSpeed = Math.Abs(int.Parse(input[3])); // the vertical speed (in m/s), can be negative.
//                var fuel = int.Parse(input[4]); // the quantity of remaining fuel in liters.
//                var rotationAngle = int.Parse(input[5]); // the rotation angle in degrees (-90 to 90).
//                var thrustPower = int.Parse(input[6]); // the thrust power (0 to 4).

//                Console.WriteLine("0 {0}", verticalSpeed > 36 ? 4 : 0);
//            }
//        }
//    }
//}