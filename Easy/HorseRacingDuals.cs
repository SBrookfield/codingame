using System;
using System.Collections.Generic;
using System.Linq;

namespace CodinGame.Easy
{
    public class HorseRacingDuals
    {
        public static void Main()
        {
            var numberOfHorses = int.Parse(Console.ReadLine());
            var horseStrengths = new int[numberOfHorses];

            for (var i = 0; i < numberOfHorses; i++)
                horseStrengths[i] = int.Parse(Console.ReadLine());

            var horseStrengthsInOrder = horseStrengths.OrderBy(s => s).ToArray();
            var smallestDifference = horseStrengthsInOrder[numberOfHorses-1];

            for (var i = 0; i < numberOfHorses - 1; i++)
            {
                var difference = Math.Abs(horseStrengthsInOrder[i] - horseStrengthsInOrder[i + 1]);
                if (difference < smallestDifference)
                    smallestDifference = difference;
            }

            Console.WriteLine(smallestDifference);
        }
    }
}