﻿using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace CodinGame.Easy
{
    public class ChuckNorris
    {
        public static void Main()
        {
            var bits = Console.ReadLine()
                .Select(c => new BitArray(Encoding.ASCII.GetBytes(c.ToString())))
                .SelectMany(ba => ba.Cast<bool>().Take(7).Reverse())
                .ToArray();

            var message = bits
                .Select((c, i) => i == 0 || bits[i - 1] != bits[i] ? " " + (bits[i] ? "0 0" : "00 0") : "0")
                .Aggregate((c,n) => c + n);

            Console.WriteLine(message.Trim());
        }
    }
}