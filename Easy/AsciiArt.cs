﻿using System;
using System.Drawing;
using System.Linq;

namespace CodinGame.Easy
{
    public class AsciiArt
    {
        public static void Main()
        {
            var letterSize = new Size(int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
            var letters = new string[27][];
            var text = Console.ReadLine();

            for (var row = 0; row < letterSize.Height; row++)
            {
                var asciiRow = Console.ReadLine();

                for (var i = 0; i < 27; i++)
                {
                    if (letters[i] == null)
                        letters[i] = new string[letterSize.Height];
                    letters[i][row] = asciiRow.Substring(i*letterSize.Width, letterSize.Width);
                }
            }

            var letterNumbers = text.Select(c => char.ToUpper(c) - 65).Select(i => i > 25 || i < 0 ? 26 : i).ToArray();

            var output = string.Empty;

            for (var i = 0; i < letterSize.Height; i++)
                output = letterNumbers.Aggregate(output, (current, letterNumber) => current + letters[letterNumber][i]) + Environment.NewLine;

            Console.Write(output);
        }
    }
}