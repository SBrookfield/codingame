﻿using System;
using System.Linq;

namespace CodinGame.Easy
{
    public class Temperatures
    {
        public static void Main()
        {
            var numberOfTemperatures = int.Parse(Console.ReadLine());
            var orderedTemps = Console.ReadLine()
                .Trim()
                .Split(' ')
                .Select(s => string.IsNullOrEmpty(s) ? "0" : s)
                .Select(int.Parse)
                .OrderBy(Math.Abs);

            Console.WriteLine(orderedTemps.Contains(Math.Abs(orderedTemps.First())) ? Math.Abs(orderedTemps.First()) : orderedTemps.First());
        }
    }
}