﻿using System;

namespace CodinGame.Easy
{
    public class MarsLander
    {
        public static void Main()
        {
            var surfacePointCount = int.Parse(Console.ReadLine());
            for (var i = 0; i < surfacePointCount; i++)
                Console.ReadLine();

            while (true)
                Console.WriteLine("0 {0}", Math.Abs(int.Parse(Console.ReadLine().Split(' ')[3])) >= 40 ? 4 : 0);
        }
    }
}