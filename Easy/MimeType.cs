﻿using System;
using System.Collections.Generic;
using System.IO;

namespace CodinGame.Easy
{
    public class MimeType
    {
        public static void Main()
        {
            var numberOfMimeAssociations = int.Parse(Console.ReadLine());
            var numberOfFileNames = int.Parse(Console.ReadLine());
            var mimeTypesByExtension = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            for (var i = 0; i < numberOfMimeAssociations; i++)
            {
                var mimeAssociation = Console.ReadLine().Split(' ');
                mimeTypesByExtension.Add(mimeAssociation[0], mimeAssociation[1]);
            }

            for (var i = 0; i < numberOfFileNames; i++)
            {
                var extension = (Path.GetExtension(Console.ReadLine()) ?? "").TrimStart('.');
                Console.WriteLine(mimeTypesByExtension.ContainsKey(extension) ? mimeTypesByExtension[extension] : "UNKNOWN");
            }
        }
    }
}