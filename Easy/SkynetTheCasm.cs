﻿using System;

namespace CodinGame.Easy
{
    public static class SkynetTheCasm
    {
        public static void Main()
        {
            var roadLength = int.Parse(Console.ReadLine());
            var gapLength = int.Parse(Console.ReadLine());
            var platformLength = int.Parse(Console.ReadLine());

            while (true)
            {
                var speed = int.Parse(Console.ReadLine());
                var position = int.Parse(Console.ReadLine());
                var platformStart = roadLength + gapLength;
                var stoppingDistance = CalculateStoppingDistance(speed);

                Console.Error.WriteLine("Road: {0}, Gap: {1}, Platform: {2}, Speed: {3}, Position: {4}, Stopping Distance: {5}.", roadLength, gapLength, platformLength, speed, position, stoppingDistance);

                if (position >= platformStart || (stoppingDistance > platformLength && (speed - 1 > gapLength)))
                {
                    Console.WriteLine("SLOW");
                    continue;
                }

                if (speed <= gapLength)
                {
                    Console.WriteLine("SPEED");
                    continue;
                }

                if (position + speed >= platformStart)
                {
                    Console.WriteLine("JUMP");
                    continue;
                }

                Console.WriteLine("WAIT");
            }
        }

        private static int CalculateStoppingDistance(int speed)
        {
            var stoppingDistance = 0;

            for (var i = speed; i > 0; i--)
                stoppingDistance += speed;

            return stoppingDistance;
        }
    }
}