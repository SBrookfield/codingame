﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CodinGame.Easy
{
    public static class Defibrillators
    {
        public class Defibrillator
        {
            public string Name { get; set; }
            public double Longitude { get; set; }
            public double Latitude { get; set; }
        }

        public static void Main()
        {
            var currentLongitude = Console.ReadLine().ToDouble();
            var currentLatitude = Console.ReadLine().ToDouble();
            var defibCount = int.Parse(Console.ReadLine());
            var defibrillators = new List<Defibrillator>();

            for (var i = 0; i < defibCount; i++)
            {
                var defibInfo = Console.ReadLine().Split(';');
                var defibrillator = new Defibrillator
                {
                    Name = defibInfo[1],
                    Longitude = defibInfo[4].ToDouble(),
                    Latitude = defibInfo[5].ToDouble()
                };

                defibrillators.Add(defibrillator);
            }

            var closestDefibrillator = defibrillators.OrderBy(d => GetDistanceBetweenPoints(currentLongitude, d.Longitude, currentLatitude, d.Latitude)).First();
            Console.WriteLine(closestDefibrillator.Name);
        }

        private static double GetDistanceBetweenPoints(double longitudeA, double longitudeB, double latitudeA, double latitudeB)
        {
            var x = (longitudeB - longitudeA)*Math.Cos((latitudeA + latitudeB)/2);
            var y = latitudeB - latitudeA;
            return Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2))*6371;
        }

        private static double ToDouble(this string str)
        {
            return double.Parse(str.Replace(",", "."));
        }
    }
}