﻿using System;
using System.Drawing;

namespace CodinGame.Easy
{
    public static class PowerOfThor
    {
        public static void Main()
        {
            var inputs = Console.ReadLine().Split(' ');
            var light = new Point(int.Parse(inputs[0]), int.Parse(inputs[1]));
            var thor = new Point(int.Parse(inputs[2]), int.Parse(inputs[3]));

            while (true)
            {
                Console.ReadLine();

                string direction = null;

                if (thor.Y > light.Y)
                {
                    direction = "N";
                    thor.Y--;
                }
                else if (thor.Y < light.Y)
                {
                    direction = "S";
                    thor.Y++;
                }

                if (thor.X > light.X)
                {
                    direction += "W";
                    thor.X--;
                }
                else if (thor.X < light.X)
                {
                    direction += "E";
                    thor.X++;
                }

                Console.WriteLine(direction);
            }
        }
    }
}